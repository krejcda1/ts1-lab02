/**
 * some update just for master
 */
public class Krejcda1 {

    public long factorial (int n) {
        if (n > 1) return n * factorial(n - 1);
        if (n < 0) return 0;
        return 1;
    }
}
